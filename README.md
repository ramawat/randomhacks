# Random Hacks

Small scripts/snippets to make life easy.

1. backup_script
   - Run chmod +x on the backup_script to make it executable.
   - Does a differential copy of selected items on an external drive. Flexibility to specify the folders/files to include and exclude. Only executes if a certain external volume is mounted.


2. BackupDaemon
   - Save the file as a `.plist` in `/Library/LaunchDaemons` folder
   - Can be modified on a variety of flags like StartOnMount, StartInterval, etc. More info [here](http://www.launchd.info/ "LaunchD info")